import threading

import cv2
from imutils.video import VideoStream

from configuration import PI_FRAME_SIZE_WEIGHT, PI_FRAME_SIZE_HEIGHT
from loggers import logger


class VideoCamera(object):

    usingPiCamera = False
    frameSize = None
    video = None
    current_frame = None
    runner = None
    run_flag = False

    def __init__(self):
        logger.info('Init camera configuration.')
        self.usingPiCamera = False
        self.frameSize = (PI_FRAME_SIZE_WEIGHT, PI_FRAME_SIZE_HEIGHT)
        self.video = VideoStream(src="/dev/video0", usePiCamera=self.usingPiCamera, resolution=self.frameSize,
                                 framerate=24).start()
        self.runner = threading.Thread(target=self.thread_process, daemon=True)

    def __del__(self):
        logger.info('Closing RGB video...')
        self.run_flag = False
        self.video.stop()

    def start_handler(self):
        self.run_flag = True
        self.runner.start()

    def update_frame(self):
        image = self.video.read()
        image = cv2.resize(image, (PI_FRAME_SIZE_WEIGHT, PI_FRAME_SIZE_HEIGHT))
        # time.sleep(0.5)
        image = cv2.rotate(image, cv2.ROTATE_90_CLOCKWISE)
        self.current_frame = image

    def get_current_frame(self):
        return self.current_frame

    def thread_process(self):
        logger.info('Start Capturing RGB video...')
        while self.run_flag:
            # Inspire from github
            self.update_frame()
