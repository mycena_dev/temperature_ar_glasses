import subprocess
import threading

import cv2
import numpy as np
from PIL import Image

from configuration import LEPTON_FRAME_SIZE_WEIGHT, LEPTON_FRAME_SIZE_HEIGHT
from loggers import logger


class LeptonCamera(object):
    frameSize = None
    rotate_90_times = None
    FLIP = None
    current_frame = None
    runner = None
    run_flag = False

    def __init__(self):
        logger.info('Init Lepton Camera configuration.')
        self.frameSize = (LEPTON_FRAME_SIZE_WEIGHT, LEPTON_FRAME_SIZE_HEIGHT)
        self.rotate_90_times = 2
        cmd = "gst-launch-1.0 v4l2src num-buffers=-1 device=/dev/video1 ! video/x-raw,format=GRAY16_LE ! pnmenc ! multifilesink location=./tmp/test.pnm".split()
        self.FLIR = subprocess.Popen(cmd)
        self.runner = threading.Thread(target=self.thread_process, daemon=True)

    def __del__(self):
        self.run_flag = False
        self.FLIR.kill()

    def start_handler(self):
        self.run_flag = True
        self.runner.start()

    def _ktoc(self, val):
        return (val - 27315) / 100.0

    def update_row_ir_data(self):
        data = np.array(Image.open('./tmp/test.pnm'))[:120, :]
        # 嘗試不升為 (320,240)
        resized_data = cv2.resize(self._ktoc(data[:,:]), self.frameSize)
        # 使用原生 (160,120)
        # resized_data = self._ktoc(data[:, :])  # (120, 160, numpy.array)
        resized_data = cv2.rotate(resized_data, cv2.ROTATE_90_COUNTERCLOCKWISE)
        self.current_frame = resized_data

    def get_current_frame(self):
        return self.current_frame

    def thread_process(self):
        logger.info('Start Capturing IR video...')
        while self.run_flag:
            # For Lepton IR Row
            self.update_row_ir_data()  # numarray, float64
