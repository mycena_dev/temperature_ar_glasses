# Torando server
TORANDO_PORT = 9000

# number of redis pool
MAX_CONNECTION = 20

# set redis url if you want to enable redis related functions like redis_brain
REDIS_URL = '0.0.0.0'    # loecalhost
REDIS_PORT = 6379    # 6379

# set redis key-value list name
KEY_CAMERA_RGB = 'Camera_RGB'
KEY_RECO_LABEL = 'RecoLabel'
KEY_CAMERA_IR = 'Camera_IR'
KEY_CAMERA_IR_ROW = 'Camera_IR_Row'

PUBSUB_CAMERA_RGB = 'PUBSUB_Camera_RGB'
PUBSUB_CAMERA_IR = 'PUBSUB_Camera_IR'
PUBSUB_CAMERA_IR_ROW = 'PUBSUB_Camera_IR_Row'

#PI camera configuration
PI_FRAME_SIZE_WEIGHT = 320
PI_FRAME_SIZE_HEIGHT = 240

#Lepton camera configuration
LEPTON_FRAME_SIZE_WEIGHT = 320
LEPTON_FRAME_SIZE_HEIGHT = 240
# LEPTON_FRAME_SIZE_WEIGHT = 320
# LEPTON_FRAME_SIZE_HEIGHT = 240

#Trained_model_name
TRAINED_MODEL_NAME = 'trained_knn_model.clf'

###NestJs Server
## For production
#NESTJS_SERVER_HOSTNAME = '59.127.187.54'
#NESTJS_SERVER_PORT = 50901
## For internal-test(4F)
NESTJS_SERVER_HOSTNAME = '192.168.31.188'
NESTJS_SERVER_PORT = 50901 
## For self-test
#NESTJS_SERVER_HOSTNAME = '192.168.31.66' # For test
#NESTJS_SERVER_PORT = 8888
